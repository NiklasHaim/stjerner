#include <stdlib.h>
#include <time.h>
#include <vector>
#include "simple_stars.h"
using namespace std;

// Funktion til tilfældige tal
int getRandomIntExclusive(int a, int b){
    return rand()*float(b-a)/RAND_MAX + a;
}

int main() {

    int win_width = 1000;
    int win_height = 600;
    int N_stars = win_width / 10;

    // Koordinater og radius
    vector<int> xpos(N_stars);
    vector<int> ypos(N_stars);
    vector<int> rr(N_stars);

    // Array med farve
    vector<int> colors(N_stars);

    // Sæt tilfældig data for alle stjerner.
    srand(time(NULL));
    rand();
    for (int i = 0; i < xpos.size(); i++) {
      xpos[i] = getRandomIntExclusive(0, win_width);
      ypos[i] = getRandomIntExclusive(0, win_height);
      rr[i] = getRandomIntExclusive(1, 30);
    }

    //Sæt vertor med farves data
    for (int i = 0; i < N_stars; i++) {
        colors[i] = getRandomIntExclusive(0, 255);
    }
    // Lav vindue til at tegne i
    Fl_Double_Window win(win_width,win_height,"Night sky");
    win.color(FL_BLACK);

    // Lav en sky, vælg farve (0-255) og tilføj stjernene
    sky nightsky(win_width,win_height);
    for(int k = 0; k < xpos.size(); k++){
        nightsky.add_star(xpos[k],ypos[k],rr[k],colors[k]);
    }

    // Opdater vinduet og alle elementer
    win.end();
    win.show();
    return(Fl::run());
}
